import React from 'react';
import {FormLogIn} from './Form/FormLogIn.js';
import {FormSignIn} from './Form/FormSignIn.js';
import {ListeMission} from './Panel/ListeMission.js';
import {APIExtern} from './Panel/APIExtern.js';
import {NavigationPannel} from './Panel/NavigationPannel.js';
import {SignIn} from './Form/SignIn.js';
import logo from './images/gold-logo-medal-icon-yellow-star-black-white-gold-pink-badge-png-clip-art.png';
import ads from './images/index.png';
import StarRatingComponent from 'react-star-rating-component'

import 'bootstrap/dist/css/bootstrap.css';

import axios from 'axios';



export class MainPage extends React.Component {
  constructor(props){
	super(props);
	this.state = {isConnected : false,
				  chaine : "abcd",
				  wantsToLog : false,
				  wantsToSign : false,
				  login : undefined,
				  id : undefined,
				  key : undefined,
				  prenom : undefined,
				  nom : undefined,
				  domaine : undefined,
				  profil : '',
				  listefollower : [],
				  listefollowed : [],
				  loaded : false,
				  showFollower : false,
				  showFollowed : false,
				  news : [],

				};
	
	this.getConnected = this.getConnected.bind(this)
	this.setLogout = this.setLogout.bind(this)
	this.getwantsToLog = this.getwantsToLog.bind(this)
	this.setwantsToLog = this.setwantsToLog.bind(this)
	this.getwantsToSign = this.getwantsToSign.bind(this)
	this.setwantsToSign = this.setwantsToSign.bind(this)

	this.getDomaine = this.getDomaine.bind(this)
	this.setDomaine = this.setDomaine.bind(this)
	
	this.setLogin = this.setLogin.bind(this)
	this.setId = this.setId.bind(this)
	this.setKey = this.setKey.bind(this)

	this.setShowFollower = this.setShowFollower.bind(this)
	this.setShowFollowed = this.setShowFollowed.bind(this)
	
	this.getLogin = this.getLogin.bind(this)
	this.getId = this.getId.bind(this)
	this.getKey = this.getKey.bind(this)

	var url;
	if(this.state.domaine === "info"){
  		url = 'http://newsapi.org/v2/everything?q=informatique&apiKey=a6a22c1a8f284051865687f4ebbb1b6d';
  	}

  	else{
  		if(this.state.domaine === "maths"){
  			url = 'http://newsapi.org/v2/everything?apiKey=a6a22c1a8f284051865687f4ebbb1b6d&q=mathematics'
              ;
  		}else{
	  		if(this.state.domaine !== undefined){
	  		url = 'http://newsapi.org/v2/everything?q='+this.state.domaine+'&apiKey=a6a22c1a8f284051865687f4ebbb1b6d';	
	        }	
	        else{
	        	url = 'http://newsapi.org/v2/top-headlines?country=fr&apiKey=a6a22c1a8f284051865687f4ebbb1b6d';  
	        }
    	}
  	}

	
    var req = new Request(url);

	fetch(req)
	    .then(reponse => reponse.json())
	    .then(data => {
	    	this.setNews(data);
	})

	axios.get("http://localhost:8080/MiniProjet/getnote",
        { params: { 
          id : this.state.id,}
        })
      .then(response => {this.setState({noteMoyenne : response.data.value})})
      .catch(error => console.log(error))



}
	
	setNews(data){
    	this.setState({news : data.articles})
	}

  getConnected(){
  	var url;

  	if(this.state.domaine === "info"){
  		url = 'http://newsapi.org/v2/everything?language=fr&q=informatique&apiKey=a6a22c1a8f284051865687f4ebbb1b6d';
  	}

  	else{
  		url = 'http://newsapi.org/v2/everything?' +
              'language=fr&'+
              
              'q='+this.state.domaine+'&' +
              'apiKey=a6a22c1a8f284051865687f4ebbb1b6d';	
  	}
	  	
	var req = new Request(url);

	fetch(req)
	    .then(reponse => reponse.json())
	    .then(data => {
	    	this.setNews(data);
		})

	axios.get("http://localhost:8080/MiniProjet/getnote",
        { params: { 
          id : this.state.id,}
        })
      .then(response => {this.setState({noteMoyenne : response.data.value})})
      .catch(error => console.log(error))


	this.setState({isConnected : true,
					wantsToLog : true,
					wantstoSign : true,})
  }

  setShowFollower(){
	this.setState({showFollower : false})
  }
  setShowFollowed(){
	this.setState({showFollowed : false})
  }

  setLogout(){
	axios.get("http://localhost:8080/MiniProjet/logout",
	  { params: { key: this.state.key}
	  })
	.then(response => {this.checkLogout(response)})
	.catch(error => console.log(error))
	this.setState({isConnected : false})
  }
  checkLogout(res){
	if(res.data.status === "OK"){
	  this.setState({isConnected : false,
					wantsToLog : false,
					wantsToSign : false,
					login : undefined,
					id : undefined,
					key : undefined,
					prenom : undefined,
					nom : undefined,
					domaine : undefined, })
	}
	else{
	  alert(res.data.desc)
	}
  }
  
  getwantsToLog(){
	this.setState({wantsToLog : true});
  }
  setwantsToLog(){
	this.setState({wantsToLog : false})
  }
  getwantsToSign(){
	this.setState({wantsToSign : true});
  }
  setwantsToSign(){
	this.setState({wantsToSign : false})
  }
  
  setLogin(value){
	this.setState({login : value}); 
  }
  setId(value){
	this.setState({id : value});
  }
  setKey(value){
	this.setState({key : value});
  }
  setDomaine(value){
  	this.setState({domaine : value})
  }

  getLogin(){
	return this.state.login 
  }
  getId(){
	return this.state.id
  }
  getKey(){
	return this.state.key
  }
  getDomaine(){
  	return this.state.domaine
  }
  getListeFollowed(){
	return this.state.listefollowed
  }

  getListeFollower(){
	return this.state.listefollower
  }
  
  
  render(){

	return(
	  <div className ="col-md-12">  
		
		{this.state.wantsToSign?
		  
		<FormSignIn setwantsToSign = {this.setwantsToSign}
					getwantsToLog={this.getwantsToLog}/>
		  :
		<div></div>}
		
		{this.state.wantsToLog?
		  <FormLogIn  setwantsToLog = {this.setwantsToLog}
					  getConnected = {this.getConnected}
					  getwantsToSign = {this.getwantsToSign}
					  setLogin = {this.setLogin}
					  setId = {this.setId}
					  setKey = {this.setKey}
					  setDomaine = {this.setDomaine}
		  />
		  :
		 <div></div>}

		<div className="row entete">
			<div className="col-sm-4 logoCadre">
				<img className = "logo" src={logo} alt="Pepe The Frog"/>
			</div>
		  
			<div className="col-sm-6">
				{this.state.isConnected?
				<div className="col-sm-12">
					<div className="col-sm-12 zone">
						<div className = "row">
						   	<div className = "col-sm-8 center profileLogin">
							   	Bonjour {this.state.login} ! 
						   	</div>
						   	<div className = "col-sm-4 center">
							   	
						   	</div>
						</div>

						<div className = "row">
								<div className = "col-sm-8 center profileScore">
								   	Votre Score 
							   	</div>
							   	<div className = "col-sm-4 center">
							   	</div>
						</div>

						<div className = "row">
							   	<div className="col-sm-8 center profileStar">
								   	<StarRatingComponent
				                      name={"star given"}
				                      value={this.state.noteMoyenne}
				                      editing={false}
				                    />
							   	</div>
							   	<div className="col-sm-4 ">
								   	
							   	</div>
						</div>


			 		</div>
				</div>
				:
				<div></div>
				}

				
	  		</div>
	  		<div className="col-sm-2">
	  			<div className="row">
	  				<div className="col-sm-12">
			            <NavigationPannel login={this.getwantsToLog} 
			                              logout={this.setLogout} 
			                              isConnected={this.state.isConnected}/>
		            </div>
	            </div>
	            <div className="row">
		            <div className = "col-sm-12">
			            <SignIn isConnected ={this.state.isConnected} 
			                    getwantsToSign={this.getwantsToSign} 
			                    getwantsToLog={this.getwantsToLog}/>
		           	</div>
	           	</div>
            </div>
		</div>
		
		<div className="row">
		  	<div className="col-sm-3 left-panel">
				<APIExtern news={this.state.news} getDomaine = {this.getDomaine}/>
		  	</div>
		  	<div className="col-sm-7">
				<ListeMission 
					isConnected = {this.state.isConnected}
					getLogin = {this.getLogin}
					getId = {this.getId}
					getKey = {this.getKey}
					getDomaine = {this.getDomaine}
					refreshMainPage = {this.refreshMainPage}/>
		  	</div>
		  	<div className="col-sm-2 right-panel">
				<img className = "ads" src={ads} alt="ads"/>
			</div>
		</div>
		
	  
	</div>
	);
  } 
}

