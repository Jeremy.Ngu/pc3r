import React from 'react';


export class HLink extends React.Component{

  refresh(){
  }

  
  render(){
    return(
      <div className = "row">
        <div className = "col-sm-12 spaceTop">
          <a className="highlight btn col-sm-12" href={this.props.article.url}>
              <div className="row col-sm-12">
                <div className = "articleURL">
                  {this.props.article.title}
                </div>
              </div>
              <div className = "row col-sm-12">
                <div className="articleSource">
                  source : {this.props.article.source.name}
                </div>
              </div>
          </a>
        </div>
      </div>
          )
  }
}