import React from 'react';


export class NavigationPannel extends React.Component{
  
  render(){
    return(
      <nav>
        {this.props.isConnected? 
          <button className="btnLogMainPageBlue" onClick={() => this.props.logout()}>Logout</button> 
          : 
          <button className="btnLogMainPageBlue" onClick={() => this.props.login()}>Login</button>}
      </nav>
    )
  }
}

