import React from 'react';
import {HLink} from './API/HLink.js';

export class APIExtern extends React.Component{

  
  
  render(){
  	return(
          <div className = "row">
            <div className = "col-sm-12">
              <div className = "row">
                <div className = "col-sm-12">
                  <div className = "newsprovider">
                  {this.props.getDomaine() === "" || this.props.getDomaine() === undefined?
                    "News about everything in France "
                    :
                    "News about " + this.props.getDomaine() 
                  }
                  <div className = "providerName">
                    provided by newsapi.org
                  </div>
                  </div>
                </div>
              </div>
              <div className = "row">
                <div className = "col-sm-12">
                  {this.props.news.map((article,i) => <HLink key = {i} article = {article}/>)}
                </div>
              </div>  
              
            </div>
          </div>
          )
  }
}