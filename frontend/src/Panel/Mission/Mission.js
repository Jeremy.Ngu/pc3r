import React from 'react';

import {EnvoyerReponse} from './EnvoyerReponse.js';
import {Reponse} from './Reponse.js'
import axios from "axios"

export class Mission extends React.Component{
  constructor(props){
    super(props);
    // alert(JSON.stringify(this.props));
    
    // alert(JSON.stringify(this.props.mission));

    this.state = {loaded : false,
      refresh : false,
      showComment : false,
      validate : undefined,
      loadName : "Commentaires",
      profilaffiche : "",
      missionDisplay : false,

      reponses : this.props.mission.reponses,
      nickname : "",
      userinfo : "",
      listeReponses : [],
      domaineIsSame : true,
      disabledButton : false,



    }
    

    this.closetheprofile = this.closetheprofile.bind(this)
    this.checkComm = this.checkComm.bind(this);
    this.deleteReponse = this.deleteReponse.bind(this)
    this.addReponse = this.addReponse.bind(this)

  }



    getuserinfo(resp){
      this.setState({userinfo : resp.data})
      this.setState({nickname : resp.data.Login})
    }


    refresh(){
      this.setState({refresh:true})
      if (typeof this.state.listeMiss !== 'undefined'){
        this.setState({reponses : this.state.listeMiss.map((mission) =>
          <mission content= {mission.mission}
          />)})
        
      }
    }

    loadCommentaire(){
      this.setState({loaded:false})
      this.setState({refresh:false})
      this.refresh();
      this.setState({loaded:true});
      this.setState({loadName : "Refresh"})

    }

    comment(){
      this.setState({repondre : true});
      this.loadCommentaire();
    }
    
    notcomment(){
      this.setState({repondre : false});
    }
    

    handleChange(e){
      this.setState({ mission: e.target.value });
    }

    checkComm(){
      this.props.refresh();
      // alert("Mission")
    }


    deleteReponse(rep_id){
      axios.get("http://localhost:8080/MiniProjet/removereponse",
        { params: { key : this.props.getKey(),
          parent : rep_id}
        })
      .then(response => {this.deleteAndUpdate(rep_id)})
      .catch(error => console.log(error))
    }

    deleteAndUpdate(rep_id){
      var deleteIndex
      var doDelete = false
      for(let i = 0; i < this.state.reponses.length;i++){
        if(this.state.reponses[i].reponse_id === rep_id){
          deleteIndex = i;
          doDelete = true
          break;
        }
      }

      if(doDelete){
        this.state.reponses.splice(deleteIndex, 1)
      }

      this.forceUpdate();
      // alert(JSON.stringify(this.state.reponses))
    }

    addReponse(mess_id, content){
      axios.get("http://localhost:8080/MiniProjet/addreponse",
            { params: { key: this.props.getKey(),
                        parent : mess_id,
                        content : content,
                        }
            })
            .then( reponse => this.addReponseAndUpdate())
            .catch(error => console.log(error))
    }

    addReponseAndUpdate(){
      this.props.refresh()
    }



    showMission(){
      this.setState({missionDisplay : true,
                    disabledButton : true})
      this.loadCommentaire()
    }

    closeMission(){
      this.setState({missionDisplay : false})
    }

    closetheprofile(){
      this.setState({showprofile : false});
    }


    componentWillReceiveProps(){

      // this.state.reponses = this.props.mission.reponses
      this.setState({reponses : this.props.mission.reponses})

      if(this.props.getId() === this.props.mission.author["user id"]){
        //this.state.validate = true;

        this.setState({validate : true})
      }else{
        // this.state.validate = false;
        this.setState({validate : false})

      }


      if(this.props.getDomaine() === this.props.mission.domaine){
        // this.state.domaineIsSame = true;
        this.setState({domaineIsSame : true})
      }else{

      }
    }

    render(){
      return(

        (this.props.displayAll || (!this.props.displayAll && this.state.validate)) || !this.props.isConnected ?
        <div className="row">
          <div className ="col-sm-12 mission bottom-border-mission">
              
              <div className = "row">
                <div className="col-sm-2 ">
                  <button className="btnProfile" >{this.props.mission.author.login}</button>    
                </div>
                
                <div className="col-sm-10 right-side">
                  {this.props.mission.date}
                </div>
              </div>

              <div className = "row">
                <div className = "col-sm-3">
                </div>
                <div className = "col-sm-6">
                  {this.props.mission.statut === "true"?
                    <div> 
                      <div className = "col-sm-12">
                        <div className="missionSuccessful center">Mission accomplie</div> 
                      </div>
                    </div>
                    : 
                    <div>
                      <div className = "col-sm-12">
                        <div className = "missionAwaiting center">Mission en attente de réponse</div>
                      </div>
                    </div>
                  }
                </div>

                <div className = "col-sm-3 right-side">
                  {this.props.mission.author["user id"] === this.props.getId()?
                    <div>
                    <button className="deleteMission" onClick={() => this.props.deleteMission(this.props.mission.mission_id)}> Supprimer </button>
                    <div className = "row missionDomaine">
                      <div className = "col-sm-12 right-side">
                        Domaine : {this.props.mission.domaine}
                      </div>
                    </div>
                    </div>
                    :
                    <div className = "missionDomaine">
                      <div className = "col-sm-12 right-side">
                        Domaine : {this.props.mission.domaine}
                      </div>
                    </div>
                  }
                </div>
              </div>

              

              <div className = "row missionTitre">
                <div className = "col-sm-12 center">
                  Titre : {this.props.mission.titre}
                </div>
              </div>



              <div className = "row">
                <div className = "col-sm-12 missionDescription center">
                  Description : {this.props.mission.description}
                </div>
              </div>

              <div className = "row spaceTop3x">
                <div className = "col-sm-12 missionDescription center">
                  Enonce : {this.props.mission.content}
                </div>
              </div>
              

              <div className = "row">
                <div className = "col-sm-12 spaceTop3x spaceBot3x">
                  
                  {(this.state.reponses !== undefined && this.state.reponses.length > 0)?
                    <div className = "row">
                      <div className = "col-sm-12 spaceTop2x spaceBot">
                        <h1 className ="reponseh1 center"> Réponses </h1>
                      </div>
                    </div>
                    :<div></div>

                  }
                  


                  {typeof this.state.reponses !== 'undefined'?

                    this.state.reponses.map((reponse,i) =>
                      <Reponse  
                                key = {i}
                                isConnected = {this.props.isConnected}
                                refresh = {this.props.refresh}
                                mission_id = {this.props.mission.mission_id} 
                                getId = {this.props.getId} 
                                getKey = {this.props.getKey} 
                                domaineIsSame={this.state.domaineIsSame} 
                                validate={this.state.validate} 
                                reponse={reponse} 
                                refreshMainPage={this.props.refreshMainPage} 
                                deleteReponse = {this.deleteReponse}
                                />)
                    :<div></div>
                  }



                  {(this.state.domaineIsSame && this.props.mission.statut !== "true") && this.props.isConnected?
                    <EnvoyerReponse addReponse = {this.addReponse} checkComm={this.checkComm} mission_id = {this.props.mission.mission_id} getKey = {this.props.getKey}/>
                    : <div></div>
                  }
                </div>
              </div>              


              
            
          </div>
        </div>
        :<div></div>
      


          )

    }
  }

  /* 

  <div className = "row">
                <div className = "col-sm-12">
                  {this.state.missionDisplay?
                      <div className = "row">
                        <div className = "col-sm-12">
                        {this.state.domaineIsSame?
                          this.state.repondre?
                            <div className="row">
                              <div className = " col-sm-12 ">
                                <label for="mission">Nouveau Commentaire :</label>
                                <textarea  className="commentaire" rows="5" cols = "40" onChange={this.handleChange.bind(this)} type="text" id="search" name="search" rows="2"/>
                                <input
                                className = "btnpublish"
                                type="button"
                                value="Publier"
                                onClick={() => this.handleClick()}/>
                                <button className="btnnotrepondre" onClick={() => this.notcomment()} >Annuler</button>
                              </div>
                            </div>
                            
                            :<button className="btnCommenter" onClick={() => this.comment()} >Commenter</button>
                          :<div></div>
                        }
                            

                        {this.state.loaded?
                          <div className ="row">
                            <div className = "col-sm-12">
                              {typeof this.state.reponses != 'undefined'?
                                this.state.reponses.map((reponse) =>
                                  <Reponse getKey = {this.props.getKey} validate={this.state.validate} reponse={reponse} refreshMainPage={this.props.refreshMainPage} />)
                                :<div></div>
                              }
                            </div>
                          </div>
                          :<div></div>
                        }
                        </div> 
                      </div>
                    :<div></div>
                  }
                </div>
              </div>

*/