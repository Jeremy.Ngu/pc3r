import React from 'react';


export class EnvoyerMission extends React.Component{
  constructor(props){
    super(props);
    this.state = {isSet : false,mission : '',
                  button : true,
                  domaine : 'maths',
                  titre : '',
                  description : '',
                  content : '',

                  };
  }

  handleChange(e){
    this.setState({[e.target.name]: e.target.value });
  }

  handleSelectDomaine(e){
    this.setState({domaine : e.target.value})
  }
  handleClick(){
    this.setState({ isSet : true });
    this.props.addMission(this.state.domaine, this.state.titre, this.state.description, this.state.content)
    this.setButtonTrue()
  }
  
  setButtonFalse(){
    this.setState({button : false})
  }

  setButtonTrue(){
    this.setState({button : true})
  }
  
  render(){
    return(
      <div className = "row">
        <div className = "col-sm-12">
          {this.props.isConnected?
                
                this.state.button?
                
                <div className = "row">
                  <div className = "col-sm-12">
                    <div className = "center">
                      <button className="btnCreateMission" onClick={() => this.setButtonFalse()}>Créer une Mission</button>
                    </div>
                  </div>
                </div>
                :
                <div className = "row">
                  <div className = "col-sm-12">
                    <div div className="popup-addMission sendMission">
                        <div className="col-sm-12"> 
                          <h3  className="register-heading">Ajouter une Mission</h3>
                        </div>



                        <div className="row">
                          <div className="col-sm-12">
                            <div className ="row">
                              <div className="col-sm-3">
                                <div className="form-group ">
                                  <input type="text" name="titre" className="form-text" placeholder="Titre *" value = {this.state.titre} onChange={this.handleChange.bind(this)}/>  
                                </div>
                                <div className="form-group">
                                  <input type="text" name="description" className="form-text" placeholder="Description *" value = {this.state.description} onChange={this.handleChange.bind(this)}/>
                                </div>
                              </div>
                            
                              <div className="col-sm-6">
                              </div>

                              <div className="col-sm-3">
                                <div className="form-group">
                                  <input type="text" name="domaine" className="form-text" placeholder="Description *" value = {this.state.domaine} onChange={this.handleChange.bind(this)}/>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="row">
                          <div className="col-sm-12 form-group">
                            <textarea type="text" rows="6" name="content" className="form-content" placeholder="Contenu *" value = {this.state.contenu} onChange={this.handleChange.bind(this)}/>
                          </div>
                        </div>
                        
                        <div className = "row">
                          <div className = "col-sm-12">
                            <div className = "row">
                              <div className = "col-sm-3">
                                <button className="btnFormCancel" onClick={() => this.setButtonTrue()} >Annuler</button>
                              </div>
                              
                              <div className = "col-sm-5"/>  

                              <div className = "col-sm-4">
                                
                                <input type="button" className="btnFormSubmitMiss right" onClick={() => this.handleClick()} value = "Envoyer"/>
                              </div>
                            </div>
                          </div>
                        </div>


                        
                    </div>
                  </div>
                </div>

          :<div></div>}
        </div>    
      </div>
        )
  }
}
