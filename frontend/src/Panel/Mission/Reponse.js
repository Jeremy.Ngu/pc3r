import React from 'react';
import axios from "axios"
import StarRatingComponent from 'react-star-rating-component'

export class Reponse extends React.Component{
  constructor(props){
    super(props);
    // alert(JSON.stringify(this.props));
    
    // alert(JSON.stringify(this.props.mission));

    this.state = {loaded : false,
      refresh : false,
      showComment : false,
      validate : false,
      loadName : "Commentaires",
      profilaffiche : "",
      showprofile : false,

      commentaires : this.props.reponse.commentaires,
      nickname : "",
      userinfo : "",
      listeReponses : [],

      noteReceived : 0,
      noteMoyenne : 0,
      hasNoted : false,
      note : null,
    }

    
      for(var i = 0 ; i < this.props.reponse.userNoted.length ; i++){
        if(this.props.reponse.userNoted[i] === this.props.getId()){
          this.state.hasNoted = true
          this.state.note = this.props.reponse.userNotedValues[i]
        }
      }

      var cpt = 0.0;
      var valueReceived = 0.0
      for(var i = 0 ; i < this.props.reponse.userNoted.length ; i++){
        valueReceived += this.props.reponse.userNotedValues[i]
      }
      this.state.noteReceived = valueReceived/cpt



      axios.get("http://localhost:8080/MiniProjet/getnote",
        { params: { 

          id : this.props.reponse.author["user id"],}
        })
      .then(response => {this.setState({noteMoyenne : response.data.value})})
      .catch(error => console.log(error))


    }

    getuserinfo(resp){
      this.setState({userinfo : resp.data})
      this.setState({nickname : resp.data.Login})
    }


    refresh(){
      this.setState({refresh:true})
      if (typeof this.state.listeMiss !== 'undefined'){
        this.setState({reponses : this.state.listeMiss.map((reponse) =>
          <reponse content= {reponse.reponse}
          />
          )})
        /*
        this.state["reponses"] = this.state.listeMiss.map((reponse) =>
          <reponse content= {reponse.reponse}
          />
          )
        */
      }
    }

    loadCommentaire(){
      this.setState({loaded:false})
      this.setState({refresh:false})
      this.refresh();
      this.setState({loaded:true});
      this.setState({loadName : "Refresh"})
    }

    comment(){
      this.setState({repondre : true});
      this.loadCommentaire();
    }
    
    notcomment(){
      this.setState({repondre : false});
    }
    

    handleChange(e){
      this.setState({ reponse: e.target.value });
    }

    handleClick(){

      axios.get("http://localhost:8080/MiniProjet/addcomment",
        { params: { key : this.props.getter.getKey(),
          content : this.state.mission,
          parent : this.props.mission.mission_id}
        })
      .then(response => {this.checkComm(response)})
      .catch(error => console.log(error))


    }

    checkComm(res){
      axios.get("http://localhost:8080/MiniProjet/getreponse",
        { params: { key : this.props.getter.getKey(),
          id : this.props.mission.mission_id}
        })
      .then(response => {this.updateComm(response)})
      .catch(error => console.log(error))
    }

    updateComm(res){
      this.setState({comments:res.data.comments}) 
    }


    showtheprofile(){
      this.setState({showprofile : true,
        profilaffiche : this.props.login,})
    }

    closetheprofile(){
      this.setState({showprofile : false});
    }

    validate(){
      axios.get("http://localhost:8080/MiniProjet/responsevalidation",
        { params: { key: this.props.getKey(),
          bool : true,
          mess_id : this.props.reponse.reponse_id}
        }).then(response => {this.props.refresh()})
      .catch(error => console.log(error))

      this.setState({validate:true})
    }

    refuse(){
      axios.get("http://localhost:8080/MiniProjet/responsevalidation",
        { params: { key: this.props.getKey(),
          bool : false,
          mess_id : this.props.reponse.reponse_id}
        }).then(response => {this.props.refresh()})
      .catch(error => console.log(error))

      this.setState({validate:false})
    }

    noter(nextValue, prevValue, name){


        axios.get("http://localhost:8080/MiniProjet/addnote",
        { params: { key: this.props.getKey(),
          id : this.props.reponse.author["user id"],
          value : nextValue,
          parent : this.props.reponse.reponse_id}
        }).then(response => {this.props.refresh()})
      .catch(error => console.log(error))
      
      this.setState({note:nextValue,
                    hasNoted:true})

      axios.get("http://localhost:8080/MiniProjet/getnote",
        { params: { 

          id : this.props.reponse.author["user id"],}
        })
      .then(response => {this.setState({noteMoyenne : response.data.value})})
      .catch(error => console.log(error))

    }

    delete(){
      this.props.deleteReponse(this.props.reponse.reponse_id)
    }

    adjustStar(nextValue, prevValue, name){
      this.setState({note : nextValue})
    }

    adjustStarOut(nextValue, prevValue, name){
      this.setState({note : 0})
    }

    componentWillReceiveProps(){

      for(var i = 0 ; i < this.props.reponse.userNoted.length ; i++){
        if(this.props.reponse.userNoted[i] === this.props.getId()){
          this.state.hasNoted = true
          this.state.note = this.props.reponse.userNotedValues[i]
        }
      }

      var cpt;
      var valueReceived = 0
      for(var i = 0 ; i < this.props.reponse.userNoted.length ; i++){
        valueReceived += this.props.reponse.userNotedValues[i]
      }
      this.state.noteReceived = valueReceived
    }


    render(){
    return(


      <div className="row">        
        <div className ="col-sm-12">
          <div className = "cadreReponse">
            <div className = "restrainMission">
              <div className = "row">
                <div className="col-sm-2 ">
                  <div>{this.props.reponse.author["login"]}</div>
                </div>
                <div className="col-sm-10 right-side">
                  Date : {this.props.reponse.date}
                </div>
              </div>

              <div className = "row">
                <div className="col-sm-4 left-side"> 
                  <StarRatingComponent
                        name={"star given"}
                        value={this.state.noteMoyenne}
                        editing={false}
                      />
                </div>
                <div className="col-sm-8">
                </div>
              </div>

              <div className = "row">
                <div className="col-sm-2">
                  {this.props.reponse.statut === "true"?
                    <div className = "sucessful">Validé !</div>
                    :this.props.reponse.statut=== "false"?<div className = "refused">Refusé</div>:<div></div>}
                </div>
                <div className="col-sm-8">
                </div>
                <div className="col-sm-2 right-side">
                  {this.props.getId() === this.props.reponse.author["user id"]?
                    <button className = "deleteReponse" onClick={() => this.delete()}>Supprimer</button>
                    :
                    <div></div>}
                </div>
              </div>


              <div className = "row spaceTop3x">
                <div className = "col-sm-12 center">
                  {this.props.reponse.content}
                </div>
              </div>


              <div className = "row">
                <div className = "col-sm-12">
                {this.props.validate && this.props.reponse.statut === "null"?
                  <div className = "spaceTop3x">
                    <div className = "row center">
                      <div className = "col-sm-3">
                      </div>

                      <div className = "col-sm-3">
                        <button className="btnValidate" onClick={() => this.validate()} >Valider</button>
                      </div>

                      <div className = "col-sm-3">
                        <button className="btnRefuse" onClick={() => this.refuse()} >Refuser</button>
                      </div>

                      <div className = "col-sm-3">
                      </div>

                    </div>
                  </div>
                :<div></div>
                }
                </div>
              </div>

              <div className = "row spaceTop2x spaceBot2x">

                <div className = "col-sm-12 right-side">

                  {this.state.hasNoted?
                    <div>
                      <div className = "row">
                        <div className = "col-sm-12">
                          Note donnée
                        </div>
                      </div>

                      <div className = "starFontSize">
                        <StarRatingComponent
                          name={"star given"}
                          value={this.state.note}
                          editing={false}
                        />
                      </div>
                    </div>
                    :
                    this.props.isConnected && this.props.getId() !== this.props.reponse.author["user id"]?
                    <div>                      
                      <div className = "row">
  	                    <div className = "col-sm-12 right-text">
  	                    	Noter la réponse
  	                    </div>
  	                   </div>

  	                <div className = "row">
  	                	<div className = "col-sm-12">
  		                    <div className = "starFontSize">
  		                    <StarRatingComponent
  		                      name={"star to give"}
  		                      value = {this.state.note}
  		                      editing={true}
  		                      onStarHover={this.adjustStar.bind(this)}
  		                      onStarHoverOut={this.adjustStarOut.bind(this)}

  		                      onStarClick={this.noter.bind(this)}
  		                    />
  		                    </div>
  		                </div>
  	                </div>
                    </div>
                    :
                    <div>
                      <div className = "row">
                        <div className = "col-sm-12">
                          Note Reçue
                        </div>
                      </div>

                      <div className = "starFontSize">
                        <StarRatingComponent
                          name={"star received"}
                          value={this.state.noteReceived}
                          editing={false}
                        />
                      </div>
                    </div>

                }
                  
                </div>
              </div>

          </div>
        </div>
      </div>

    </div>


  )
  }
}