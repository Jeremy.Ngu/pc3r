import React from 'react';

export class EnvoyerReponse extends React.Component{
  constructor(props){
    super(props);
    this.state = {isSet : false,mission : '',
                  button : true,
                  domaine : '',
                  titre : '',
                  description : '',
                  content : '',

                  };
    console.log(this.props)
  }

  handleChange(e){
    this.setState({[e.target.name]: e.target.value });
  }

  handleClick(){
    this.setState({ isSet : true });
    this.addReponse(this.props.mission_id, this.state.content)
    this.setButtonTrue()
  }

  addReponse(mess_id, content){
    this.props.addReponse(mess_id,content)
  }

  checkComm(){
    this.props.checkComm()
  }

  setButtonFalse(){
    this.setState({button : false})
  }

  setButtonTrue(){
    this.setState({button : true})
  }
  
  render(){
    return(

      <div className = "spaceBot2x spaceTop3x sendMissionCadre">
        {this.state.button?
          <div className = "row spaceTop2x spaceBot2x">
            <div className = "col-sm-12 center">
              <button className="btnAddReponse" onClick={() => this.setButtonFalse()}> Ajouter une réponse </button>
            </div>
          </div>

        : 
        <div className = "row">
          <div className = "col-sm-12">
            <div className="row">
              <div className="col-sm-12 form-group">
                <textarea type="text" rows="6" name="content" className="form-content" placeholder="Contenu *" value = {this.state.contenu} onChange={this.handleChange.bind(this)}/>
              </div>
            </div>
            
            <div className = "row">
              <div className = "col-sm-12">
                <div className = "row">
                  <div className = "col-sm-3">
                    <button className="btnFormCancel" onClick={() => this.setButtonTrue()} >Annuler</button>
                  </div>
                  
                  <div className = "col-sm-5"/>  
                  <div className = "col-sm-4">
                    <input type="button" className="btnFormSubmitMiss right" onClick={() => this.handleClick()} value = "Envoyer"/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>        
        }
        </div>
        )
  }
}
