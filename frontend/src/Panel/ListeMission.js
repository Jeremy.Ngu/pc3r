import React from 'react';
import {EnvoyerMission} from './Mission/EnvoyerMission.js';
import {Mission} from './Mission/Mission.js';

import axios from "axios"



export class ListeMission extends React.Component{
  constructor(props){
    super(props);
    this.state = {
                  liste : [],
                  missionEnvoyé : false,
                  
                  displayAll : true,

                 }
                    
    this.addMission = this.addMission.bind(this)
    axios.get("http://localhost:8080/MiniProjet/allmission")
      .then(response => {this.checkListe(response)})
      .catch(error => console.log(error))


    this.refresh = this.refresh.bind(this);
    this.deleteMission = this.deleteMission.bind(this);
  }

  

  checkListe(res){
    const lesMissions = (res.data.missions.map((mission) => mission)).reverse();
    this.setState({liste : lesMissions})
    this.forceUpdate()
    // alert(JSON.stringify(res))
  }


  addMission(domaine, titre, description, content){
    axios.get("http://localhost:8080/MiniProjet/addmission",
            { params: { key: this.props.getKey(),
                        titre : titre,
                        domaine : domaine,
                        description : description,
                        probleme : content,
                        }
            })
            .then(response => {this.check(response)})
            .catch(error => console.log(error))
  }

  deleteMission(mess_id){
    // alert(this.props.getKey())


    axios.get("http://localhost:8080/MiniProjet/removemission",
            { params: { key: this.props.getKey(),
                        parent : mess_id,
                        }
            })
            .then(response => {this.check(response)})
            .catch(error => console.log(error))
  }

  check(res){
    if(res.data.status === "OK"){
      // alert("Effectué")
      this.setState({missionEnvoyé : true})
    }
    else{
      alert(res.data.desc)
    }
    axios.get("http://localhost:8080/MiniProjet/allmission", {xhrFields: {withCredentials: true}},)
      .then(response => {this.checkListe(response)})
      .catch(error => console.log(error))
    // alert(JSON.stringify(this.state.liste))
  }

  refresh(){
    axios.get("http://localhost:8080/MiniProjet/allmission", {xhrFields: {withCredentials: true}},)
      .then(response => {this.checkListe(response)})
      .catch(error => console.log(error))
    // alert(JSON.stringify(this.state.liste))
  }
  
  displayMyMission(){
    this.setState({displayAll : false})
  }

  displayAllMission(){
    this.setState({displayAll : true})
  }
  
  render(){
    return(
          <div className="row">
            <div className="col-sm-12">


              <div className="row">
                <div className = "col-sm-12">
                  <EnvoyerMission 
                    isConnected ={this.props.isConnected} 
                    addMission = {this.addMission}
                    />
                </div>
              </div>

              {this.props.isConnected?
              <div className="row spaceTop3x top-border-mission">
                  <div className = "col-sm-6 center">
                    <button className = "yourMission" onClick={()=>this.displayMyMission()}> Vos Missions </button>
                  </div>
                  <div className = "col-sm-6 center">
                    <button className="allMission" onClick={()=>this.displayAllMission()}> Toutes Les Missions </button>
                  </div>
              </div>
              :<div></div>
              }
              
              <div className ="row">
                <div className="col-sm-12 spaceTop">
                  {this.state.liste.map((mission,i) =>
                    <Mission  
                              key = {i}
                              isConnected = {this.props.isConnected}
                              getter = {this.getter} 
                              refresh = {this.refresh} 
                              getId={this.props.getId} 
                              getDomaine = {this.props.getDomaine} 
                              getKey = {this.props.getKey} 
                              mission={mission} 
                              deleteMission = {this.deleteMission}
                              refreshMainPage = {this.props.refreshMainPage} 
                              displayAll = {this.state.displayAll} />)}
                </div>
              </div>
            </div>
          </div>
          )   
  }

}





    
    
