import React from 'react';
import axios from "axios"

export class ProfilePopup extends React.Component{
  constructor(props){
    super(props);
    this.state = {isFollowing : false,
                 followsyou : false,
                 listefollow : [],
                 listefollower : [],};

    console.log(this.props)
    alert(this.props.userinfo.id)
    console.log(this.props.getter)
    console.log(this.props.getter.getter)
    console.log(this.props.getter.getter.getKey())

    this.refresh();
  }

  refresh(){


  }


  getisfollowing(res){
    if(res.data.status === "OK"){
      this.setState({isFollowing : true})
    }
    else{
      this.setState({isFollowing : false})
    }
  }
  
  getfollowsyou(res){
    alert(JSON.stringify(res.data))
    if(res.data.status === "OK"){
      this.setState({followsyou : true})
    }
    else{
      this.setState({followsyou : false})
    }
  }


  follow(){
    alert(this.props.getter.getter.getKey() ,this.props.userinfo.id)
    axios.get("http://localhost:8080/3I017/addfriend",
            { params: { key : this.props.getter.getter.getKey() , id_friend : this.props.userinfo.id}
            })
            .then(response => {this.setState({isFollowing : true})})
            .catch(error => console.log(error))
    this.refresh()
    console.log(this.state)
  }
  
  unfollow(){
    axios.get("http://localhost:8080/3I017/removefriend",
            { params: { key : this.props.getter.getter.getKey() , id_friend : this.props.userinfo.id}
            })
            .then(response => {this.setState({isFollowing : false})})
            .catch(error => console.log(error))
    this.refresh()
    console.log(this.state)
  }
  
  render(){
    this.props.refreshMainPage();
    return(
      <div class="popup container profilepopup cadreReponse">
          <div class="col-sm-11"> 
            <h3  class="register-heading">Profile</h3>
          </div>
          <div class="col-sm-1"> 
            <button class = "btnClose" onClick={() => this.props.close()}>X</button>
          </div>
          
        
        {this.state.followsyou?
          <div class="col-sm-12">
            <h4 class = "relation"> Vous suit</h4>
          </div>
          :
          <div class="col-sm-12">
          </div>
        }

          <div class="col-sm-12 zone profile">
            <div class = "row">
            <div class="col-sm-2 ">
              {this.props.userinfo.Login}
            </div>
            <div class="col-sm-6 ">
             
            </div>
            <div class="col-sm-2 ">
              {this.props.userinfo.Prenom}
            </div>
            <div class="col-sm-2 ">
              {this.props.userinfo.Nom}
            </div>
            </div>
        
            <div class = "row">
            <div class="col-sm-6 ">
               Nombre de tweet
            </div>
            <div class="col-sm-3 ">
               Nombre de follow : {this.state.listefollow.length}
            </div>
            <div class="col-sm-3 ">
               Nombre de follower : {this.state.listefollower.length}
            </div>
            </div>
          </div>

          
          <div class = "col-sm-12">{this.state.isFollowing}
            {this.state.isFollowing ?
            <button class="btnUnfollow" onClick={() => this.unfollow()} >unfollow</button>
              :
            <button class="btnFollow" onClick={() => this.follow()} >follow</button>
            }
          </div>
          
    </div>)
  }
}
