import React from 'react';


import axios from 'axios'


export class FormLogIn extends React.Component{
  constructor(props){
    super(props);
    this.state = {isSet : false,
                  };
  }
  
  connection(){
    this.props.setwantsToLog()
    this.props.getConnected()
  }
  
  signin(){
    this.props.setwantsToLog()
    this.props.getwantsToSign()
  }
  
  handleChange(e){
    this.setState({[e.target.name]: e.target.value });
  }

  handleClick(){
    axios.get("http://localhost:8080/MiniProjet/login",
      { params: { login: this.state.login,
                  pw : this.state.pw}
      })
    .then(response => {this.check(response)})
    .catch(error => console.log(error))

  }

  check(res){
    // alert(JSON.stringify(res.data))
    if(res.data.status === "OK"){
      this.props.setKey(res.data.key)
      this.props.setId(res.data.id)
      this.props.setLogin(res.data.login)
      this.setState({isSet : true})
      this.props.setDomaine(res.data.domaine)
      this.props.getConnected();

    }
    else{
      alert(res.data.desc)
    }
  }
  
  render(){
    return(
      this.state.isSet === true?
      <div></div>
      :
       
    <div className="popup login-container register">
      
        <div className="col-sm-12"> 
          <h3  className="register-heading">Login</h3>
        </div>

        <div className="row form-group">
          <input type="text" className="form-text" name ="login" placeholder="Your Login *" value={this.state.login} onChange={this.handleChange.bind(this)}/>
        </div>
        <div className="row form-group">
          <input type="password" name="pw" className="form-text" placeholder="Your PassWord *" value = {this.state.pw} onChange={this.handleChange.bind(this)}/>
        </div>

        <div className = "row">
        </div>
        
        <div className = "row">
          <div className = "col-sm-4">
            <a  className = "btnForgetPwd" href="MotDePasseOublie">Mot de passe oublié</a>
          </div>  
          <div className = "col-sm-4">
          </div>
          <div className = "col-sm-4">
            <input type="button" className="btnFormSubmit right" onClick={() => this.handleClick()} value = "Connexion"/>
          </div>

        </div>

        <div className = "row">
          <div className = "col-sm-4">
            <button className="btnForgetPwd" onClick={() => this.signin()} >Créer un compte</button>
          </div>  
          <div className = "col-sm-4">
          </div>
          <div className = "col-sm-4">
            <button className="btnFormCancel right" onClick={() => this.props.setwantsToLog()} >Annuler</button>
          </div>
        </div>
        
        
        
        
          
    </div>)
  }
}
