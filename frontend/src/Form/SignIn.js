import React from 'react';


export class SignIn extends React.Component{
  
  signin(){
    this.props.getwantsToSign()       
  }
  
  render(){
    return(
      <div>
        {this.props.isConnected? 
          <></>
          : 
          <button className="btnLogMainPage" onClick={() => this.signin()}>Sign In</button>}
          
      </div>
    )
  }
  
}