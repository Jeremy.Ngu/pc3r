import React from 'react';


import axios from "axios";

export class FormSignIn extends React.Component{
  constructor(props){
    super(props);
    this.state = {isSet : false,
                  prenom : '',
                  nom : '',
                  login : '',
                  domaine : '',
                  password : '',
                  passwordAgain : '',
                 
                  loginIsGood : false,
                  passwordIsGood : false,
                  nomprenomIsGood : false};
  }
  
  handleChange(e){
    this.setState({[e.target.name]: e.target.value });
  }

  handleClick(){
    if(this.state.domaine !== ''){
      if(this.state.password === this.state.passwordAgain){
        if(this.state.login !== ''){
          if(this.state.nom !== '' &&  this.state.prenom !== ''){
              console.log(this.state.login,
                          this.state.password,
                          this.state.prenom,
                          this.state.nom)
              axios.get("http://localhost:8080/MiniProjet/create-user",
              { params: { login: this.state.login,
                          pw : this.state.password,
                          prenom : this.state.prenom,
                          nom : this.state.nom,
                          domaine : this.state.domaine,}
              })
              .then(response => {this.check(response)})
              .catch(error => console.log(error))
              
              this.props.setwantsToSign();
          }
        }
      }
    }
  
  }
  
  check(res){
    if(res.data.status === "OK"){
      this.props.setKey(res.data.key)
      this.props.setId(res.data.id)
      this.props.setLogin(res.data.login)
      this.setState({isSet : true})
      this.props.getConnected();
    }
    else{
      alert(res.data.desc)
    }
  }

  login(){
    this.props.setwantsToSign()
    this.props.getwantsToLog()
  }
  
  render(){
    return(
      
      this.state.isSet === true?
      
       <div></div>
      :
      <div className="popup login-container register">
        <div className="col-sm-12"> 
          <h3  className="register-heading">Créer un compte</h3>
        </div>

        <div className="row">
          <input type="text" name="login" className="inputLogin" placeholder="Login *" onChange={this.handleChange.bind(this)}/>
        </div>
        <div className="row">
          <input type="text" name="domaine" className="inputLogin" placeholder="Domaine *" onChange={this.handleChange.bind(this)}/>
        </div>
        
        <div className = "row">
          <div className="SignIncenter col-sm-6">
            <input type="text" className="inputLogin" name ="prenom" placeholder="Prenom *" value={this.state.prenom} onChange={this.handleChange.bind(this)}/>
          </div>
          <div className="SignIncenter col-sm-6">
            <input type="text" className="inputLogin" name ="nom" placeholder="Nom *" value={this.state.nom} onChange={this.handleChange.bind(this)}/>
          </div>
        </div>
        
        <div className = "row">
        <div className="SignIncenter col-sm-6">
          <input type="password" name="password" className="inputLogin" placeholder="Password *" onChange={this.handleChange.bind(this)}/>
        </div>
        <div className="SignIncenter col-sm-6">
          <input type="password" name="passwordAgain" className="inputLogin" placeholder="Password Again*" onChange={this.handleChange.bind(this)}/>
        </div>
        </div>
        
        <div className = "row">
        <div className = "SignIncenterButton col-sm-6">
          <input type="button" className="btnContactSubmit" onClick={() => this.handleClick()} value = "Créer un compte"/>
        </div>
        <div className = "SignIncenterButton col-sm-6">
          <button className="btnContactSubmit" onClick={() => this.props.setwantsToSign()} >Annuler</button>
        </div>
        </div>
        
        <div className = "row">
        <div className = "SignIncenterButtonPlus form-group col-sm-12">
          <button className="btnLoginSubmit" onClick={() => this.login()} >Déjà un compte ?</button>
        </div>
        
        </div>
          
    </div>
    
    )
  }
}
