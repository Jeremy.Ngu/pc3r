-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : mar. 09 juin 2020 à 12:07
-- Version du serveur :  8.0.19
-- Version de PHP : 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+01:00";
SET GLOBAL time_zone = "+01:00";


CREATE DATABASE nguyen_j;
USE nguyen_j;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `nguyen_j`
--

-- --------------------------------------------------------

--
-- Structure de la table `follow`
--

CREATE TABLE `follow` (
  `id1` int UNSIGNED NOT NULL,
  `id2` int UNSIGNED NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des amis';

--
-- Déchargement des données de la table `follow`
--

INSERT INTO `follow` (`id1`, `id2`, `approved`) VALUES
(1, 2, 1),
(2, 1, 1),
(2, 3, 0),
(2, 5, 1),
(3, 1, 0),
(3, 2, 0),
(5, 2, 0),
(5, 4, 0);

-- --------------------------------------------------------

--
-- Structure de la table `note`
--

CREATE TABLE `note` (
  `id1` int NOT NULL,
  `id2` int NOT NULL,
  `value` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `note`
--

INSERT INTO `note` (`id1`, `id2`, `value`) VALUES
(0, 1, 5),
(9, 3, 3),
(2, 1, 1),
(9, 4, 4),
(9, 6, 2),
(9, 9, 3),
(9, 6, 1),
(9, 9, 1),
(9, 9, 5),
(9, 6, 5),
(9, 9, 1),
(9, 4, 5),
(9, 9, 4),
(9, 9, 1);

-- --------------------------------------------------------

--
-- Structure de la table `session`
--

CREATE TABLE `session` (
  `sessionkey` varchar(32) NOT NULL COMMENT 'Clé de connexion de la session',
  `id` int UNSIGNED NOT NULL,
  `sdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'timestamp de création de la session',
  `root` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stockage des sessions';

--
-- Déchargement des données de la table `session`
--

INSERT INTO `session` (`sessionkey`, `id`, `root`) VALUES
('40I8E14sqLyfku4wLvdWV', 9, 1),
('62gEnex4sbazWh8O3cqz7', 9, 1),
('7E8r9JDfcCgdW8awv9GXz', 1, 1),
('85xXi3Fh48Nl5g7zc4A7v', 1, 1),
('bqYUMqAduXFmuJthok4Eh', 9, 1),
('ee6236cca3184d579aa489a64902d508', 4, 0),
('EyxAwDdevmjfd7eSZE6JW', 6, 1),
('fiFZywaUxQhKPlmLFVaqG', 9, 1),
('Gw54RzQxwFtntlpxZoBkh', 9, 1),
('KkVDlj2eaPcY2xt6teU9N', 9, 1),
('O8WwWYihU4onJEv4TGK2j', 9, 1),
('P29h3PYw6rX9QyHtm9ip3', 9, 1),
('VuWaSusoa3aGxnqqDI7sn', 9, 1),
('yWTUMylsSaZPI0nHFutDV', 9, 1);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int UNSIGNED NOT NULL,
  `login` varchar(255) NOT NULL COMMENT 'Login de l''utilisateur',
  `password` varchar(255) NOT NULL COMMENT 'Mot de passe (fct PASSWORD() appliqué)',
  `root` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Utilisateur root (true) ou non (false)',
  `nom` varchar(255) DEFAULT NULL COMMENT 'Nom utilisateur',
  `prenom` varchar(255) DEFAULT NULL COMMENT 'Prénom utilisateur',
  `domaine` varchar(255) DEFAULT NULL COMMENT 'Domaine utilisateur',
  `age` int DEFAULT NULL COMMENT 'Age de l''utilisateur (NULL = Non renseigné)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des utilisateurs';

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `root`, `nom`, `prenom`, `domaine`, `age`) VALUES
(1, 'thib', '1234', 0, 'SIMON-FINE', 'Thibaut', 'maths', 22),
(2, 'Noxi', '1', 0, NULL, NULL, 'maths', NULL),
(3, 'panda', 'azerty', 0, NULL, NULL, 'info', NULL),
(4, 'theGreatTrixie', 'qwerty', 0, NULL, NULL, 'maths', NULL),
(5, 'sparkle', 'test', 0, NULL, 'Twilight', 'maths', NULL),
(6, 'toto', 'password', 0, NULL, NULL, 'physique', NULL),
(9, 'a', 'a', 0, 'a', 'a', 'info', NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `follow`
--
ALTER TABLE `follow`
  ADD PRIMARY KEY (`id1`,`id2`),
  ADD KEY `id1` (`id1`),
  ADD KEY `id2` (`id2`);

--
-- Index pour la table `session`
--
ALTER TABLE `session`
  ADD UNIQUE KEY `unique_sessionkey` (`sessionkey`),
  ADD KEY `id` (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_login` (`login`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `session`
--
ALTER TABLE `session`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `follow`
--
ALTER TABLE `follow`
  ADD CONSTRAINT `fk_friend_idfriend` FOREIGN KEY (`id1`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_friend_iduser` FOREIGN KEY (`id2`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `fk_session_iduser` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
