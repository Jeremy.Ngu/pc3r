package bd;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

public class MongoUtil {
	
	public static String addMission(String key, String domaine, String titre, String description, String content) throws SQLException {

		int id = bd.Util.getUserIDfromKey(key);
		MongoCollection<Document> mess_col;
		
		MongoWrap wrap = DataBase.getMongoCollection("mission");
		mess_col = wrap.collection;
		
		Document query = new Document();
		query.append("user id", id);
		query.append("domaine", domaine);
		query.append("titre", titre);
		query.append("description", description);
		query.append("content", content);
		query.append("statut", "null");
		
		GregorianCalendar c = new GregorianCalendar();
		query.put("date", c.getTime());
		
		// Comment instantier une mongocollection ????
		query.put("reponses", new ArrayList<ObjectId>());
		
		mess_col.insertOne(query);	
		// Retourne l'ID du mission
		ObjectId id_mess = (ObjectId) query.get("_id");
		
		wrap.conn.close();
		return id_mess.toString();
		
		 
	}
	public static boolean userAlreadyNoted(String key, String parent) throws SQLException {
		MongoWrap wrap = DataBase.getMongoCollection("reponse");
		
		MongoCollection<Document> reponse = wrap.collection;
		int id_user = bd.Util.getUserIDfromKey(key);
		
		Document query = new Document();
		query.append("_id", new ObjectId(parent));
		
		MongoCursor<Document> rep = reponse.find(query).iterator();
		if(rep.hasNext()){
			
			Document document = rep.next();
			ArrayList<Integer> userNoted = (ArrayList<Integer>) document.get("userNoted");
			
			if(userNoted.contains(id_user)) {
				wrap.conn.close();
				return true;
			}
			else {
				wrap.conn.close();
				return false;
			}		
			
		}
		return false;

	}
	
	public static void addUserNoted(String key, String parent, int value) throws SQLException {
		MongoWrap wrap = DataBase.getMongoCollection("reponse");
		
		MongoCollection<Document> reponse = wrap.collection;
		int id_user = bd.Util.getUserIDfromKey(key);
		
		Document query = new Document();
		query.append("_id", new ObjectId(parent));
		
		MongoCursor<Document> rep = reponse.find(query).iterator();
		if(rep.hasNext()){
			
			Document document = rep.next();
			ArrayList<Integer> userNoted = (ArrayList<Integer>) document.get("userNoted");
			ArrayList<Integer> userNotedValues = (ArrayList<Integer>) document.get("userNotedValues");
			
			userNoted.add(id_user);				
			userNotedValues.add(value);
			
			document.put("userNoted", userNoted);
			document.put("userNotedValues", userNotedValues);
			
			System.out.println(document.toString());
			reponse.replaceOne(query,document);
			
			wrap.conn.close();
		}
		
	}
	
	public static String addReponse(String key, String content, String parent) throws SQLException {
		
		MongoWrap wrap1 = DataBase.getMongoCollection("mission");
		MongoWrap wrap2 = DataBase.getMongoCollection("reponse");
		
		MongoCollection<Document> mission = wrap1.collection;
		MongoCollection<Document> reponse = wrap2.collection;
		int id_user = bd.Util.getUserIDfromKey(key);
		
		Document query = new Document();
		query.append("_id", new ObjectId(parent));
		
		MongoCursor<Document> msg = mission.find(query).iterator();
		if(msg.hasNext()){
			Document document = msg.next();
			
			
			Document newMission = new Document();
			newMission.append("user id", id_user);
			newMission.append("content", content);
			GregorianCalendar c = new GregorianCalendar();
			newMission.append("date", c.getTime());
			newMission.append("reponses", new ArrayList<ObjectId>());
			newMission.append("userNoted", new ArrayList<Integer>());
			newMission.append("userNotedValues", new ArrayList<Integer>());
			newMission.append("parent", parent);
			newMission.append("statut", "null");
			
			reponse.insertOne(newMission);
			ObjectId last_id = (ObjectId) newMission.get("_id");
			
			@SuppressWarnings("unchecked")
			ArrayList<ObjectId> reponses = (ArrayList<ObjectId>) document.get("reponses");
			reponses.add(last_id);
			
			document.put("reponses",reponses);
			
			
			
			
			
			mission.replaceOne(query,document);
			
			wrap1.conn.close();
			wrap2.conn.close();
			return last_id.toString();
		}
		
		wrap1.conn.close();
		wrap2.conn.close();
		return "";
	}
	
	public static JSONObject validate(String key, String bool, String mess_id) throws SQLException, JSONException {
		
		int id = bd.Util.getUserIDfromKey(key);
		
		MongoWrap wrap1 = DataBase.getMongoCollection("reponse");
		MongoWrap wrap2 = DataBase.getMongoCollection("mission");
		MongoCollection<Document> reponse = wrap1.collection;
		MongoCollection<Document> mission = wrap2.collection;
		
		Document query = new Document();
		query.append("_id", new ObjectId(mess_id));
		MongoCursor<Document> msg = reponse.find(query).iterator();
		
		JSONObject retour = tools.ServicesTools.serviceAccepted();
		
		if(msg.hasNext()){
			
			Document document = msg.next();
			boolean ValidateRight = false;
			// Verification du droit de valider
			if(document.containsKey("parent")) {

				String id_parent = document.getString("parent");
				Document queryTemp = new Document();
				queryTemp.append("_id", new ObjectId(id_parent));
				MongoCursor<Document> msgTemp = mission.find(queryTemp).iterator();
				if(msgTemp.hasNext()) {
					
					
					Document documentTemp = msgTemp.next();
					if(documentTemp.getInteger("user id") == id) {
						ValidateRight = true;
						if(bool.equals("true")) {
							documentTemp.put("statut", "true");
							mission.replaceOne(queryTemp, documentTemp);
						}
					}
					else { 
						wrap1.conn.close();
						wrap2.conn.close();
						return tools.ServicesTools.serviceRefused("No right to validate", 100);
					}
					
				}
			}
			
			if(ValidateRight) {
				document.put("statut", bool);
				reponse.replaceOne(query,document);
				wrap1.conn.close();
				wrap2.conn.close();
				return retour;
			}
		}
		wrap1.conn.close();
		wrap2.conn.close();
		return tools.ServicesTools.serviceRefused("No message found", 100);
	}
	
	public static void removeMission(String key, String mission_id) throws SQLException{
		
		Integer user_id = bd.Util.getUserIDfromKey(key);
		
		MongoWrap wrap = DataBase.getMongoCollection("mission");
		MongoCollection<Document> mission = wrap.collection;
		Document query = new Document();
		
		query.append("_id", new ObjectId(mission_id));
		query.append("user id", user_id);
		MongoCursor<Document> msg = mission.find(query).iterator();
		
		if(msg.hasNext()) {
			removeMission(mission_id);
			
			
		}
		wrap.conn.close();
	}
	
	public static JSONObject removeReponse(String key, String reponse_id) throws SQLException, JSONException{
		
		JSONObject retour = tools.ServicesTools.serviceAccepted();
		retour = removeReponse(reponse_id);
		return retour;
	}
	
	public static void removeMission(String mission_id) {
		MongoWrap wrap1 = DataBase.getMongoCollection("mission");
		MongoWrap wrap2 = DataBase.getMongoCollection("reponse");
		
		MongoCollection<Document> mission = wrap1.collection;
		MongoCollection<Document> reponse = wrap2.collection;
		Document query = new Document();
		query.append("_id", new ObjectId(mission_id));
		
		
		MongoCursor<Document> msg = mission.find(query).iterator();
		
		if(msg.hasNext()){
			Document document = msg.next();
			
			@SuppressWarnings("unchecked")
			ArrayList<ObjectId> reponseIds = (ArrayList<ObjectId>) document.get("reponses");
			
			// System.out.println(reponseIds.size());
			for (ObjectId idReponse : reponseIds) {
				// System.out.println(idReponse.toString());
				Document queryRes = new Document();
				queryRes.append("_id", idReponse);
				reponse.deleteOne(queryRes);
			}
		}
		
		
		mission.deleteOne(query);
		
		wrap1.conn.close();
		wrap2.conn.close();
	}
	
	public static JSONObject removeReponse(String reponse_id) throws JSONException {
		
		MongoWrap wrap1 = DataBase.getMongoCollection("mission");
		MongoWrap wrap2 = DataBase.getMongoCollection("reponse");
		JSONObject retour = tools.ServicesTools.serviceAccepted();
		
		MongoCollection<Document> mission = wrap1.collection;
		MongoCollection<Document> reponse = wrap2.collection;
		Document query = new Document();
		query.append("_id", new ObjectId(reponse_id));
		
		
		MongoCursor<Document> msg = reponse.find(query).iterator();
		
		if(msg.hasNext()){
			retour.put("reponse found", "true");
			Document document = msg.next();
			String parent = (String) document.get("parent");
			if(parent!=null){
				Document queryParent = new Document();
				queryParent.append("_id", new ObjectId(parent));
				
				
				MongoCursor<Document> msgParent = mission.find(queryParent).iterator();
				Document newDocMiss = new Document();
				if(msgParent.hasNext()){
					newDocMiss = msgParent.next();
					@SuppressWarnings("unchecked")
					ArrayList<ObjectId> reponses = (ArrayList<ObjectId>) newDocMiss.get("reponses");
					reponses.remove(new ObjectId(reponse_id));
				}
				mission.replaceOne(queryParent, newDocMiss);
			}
		}
		
		
		reponse.deleteOne(query);
		wrap1.conn.close();
		wrap2.conn.close();
		return retour;
	}

	public static JSONObject getMissionById(String mission_id) {
		MongoWrap wrap = DataBase.getMongoCollection("mission");
		try {

		
			
		MongoCollection<Document> mission = wrap.collection;
	
		
		Document query = new Document();
		query.append("_id", mission_id);
		
		
		MongoCursor<Document> msg = mission.find(query).iterator();
		JSONObject json = new JSONObject();
		JSONObject auteur = new JSONObject();
		
		if(msg.hasNext()) {
			Document document = msg.next();
			auteur.put("user id", document.get("user id"));
			auteur.put("login", bd.Util.getUserLogin(Integer.parseInt(document.get("user id").toString())));
			json.put("author", auteur);
			json.put("mission_id", document.get("_id"));
			json.put("content", document.get("content"));
			json.put("date", document.get("date"));
		}
		wrap.conn.close();
		return json;
		} catch (JSONException e) {
			wrap.conn.close();
			return tools.ServicesTools.serviceRefused("Erreur JSON", 100);	
		} catch (NumberFormatException e) {
			wrap.conn.close();
			return tools.ServicesTools.serviceRefused("Erreur JAVA", 10000);	
		} catch (SQLException e) {
			wrap.conn.close();
			return tools.ServicesTools.serviceRefused("Erreur SQL", 1000);	
		}
		
		
	}

	
	public static JSONArray getMissionsUser(int id_user) throws JSONException, SQLException {
		return getMissionsUser(id_user, 0);
	}
	
	public static JSONArray getMissionsUser(String key, int max_value) throws SQLException, JSONException {
		int id_user = bd.Util.getUserIDfromKey(key);
		return getMissionsUser(id_user, max_value);
	}
	
	public static JSONArray getReponse(Document mess) throws JSONException, NumberFormatException, SQLException {
		
		
		JSONArray reponses = new JSONArray();		
		@SuppressWarnings("unchecked")
		ArrayList<ObjectId> reponseIds = (ArrayList<ObjectId>) mess.get("reponses"); 
		if(reponseIds.size()!=0){
			for (Object idReponse : reponseIds) {
				
				MongoWrap wrap = DataBase.getMongoCollection("reponse");
				MongoCollection<Document> reponse = wrap.collection;
				
				Document query = new Document();
				query.append("_id", idReponse);
				MongoCursor<Document> msg = reponse.find(query).iterator();
						
				JSONObject json = new JSONObject();
				JSONObject auteur = new JSONObject();
				
				if(msg.hasNext()) {
					Document document = msg.next();
					auteur.put("user id", document.get("user id"));
					auteur.put("login", bd.Util.getUserLogin(Integer.parseInt(document.get("user id").toString())));
					json.put("author", auteur);
					json.put("reponse_id", document.get("_id"));
					json.put("content", document.get("content"));
					json.put("statut", document.get("statut"));
					json.put("date", document.get("date"));
					json.put("userNoted", document.get("userNoted"));
					json.put("userNotedValues", document.get("userNotedValues"));
				}
				
				reponses.put(json);
				wrap.conn.close();
			}
		}
		
		return reponses;
		
	}
	
	public static JSONArray getMissionsUser(int user_id, int max_value) throws JSONException, SQLException{
		MongoWrap wrap = DataBase.getMongoCollection("mission");
		MongoCollection<Document> mission = wrap.collection;
		BasicDBObject query = new BasicDBObject("user id",user_id);
		MongoCursor<Document> msg = mission.find(query).iterator();
		
		JSONArray userMissions = new JSONArray();
		while(msg.hasNext()){
			JSONObject json = new JSONObject();
			JSONObject auteur = new JSONObject();
			Document document = msg.next();
			auteur.put("user id", document.get("user id"));
			auteur.put("login", bd.Util.getUserLogin(Integer.parseInt(document.get("user id").toString())));
			
			
			json.put("author", auteur);
			json.put("mission_id", document.get("_id"));
			json.put("content", document.get("content"));
			json.put("date", document.get("date"));
			json.put("reponses", getReponse(document));
			
			
			if(document.get("parent")!=null){
				String idparent = (String) document.get("parent");
				json.put("parent", idparent);
				JSONObject mission_parent = getMissionById(idparent);
				json.put("parent_author", mission_parent.get("author"));
			}
			userMissions.put(json);
		
		}
		
		wrap.conn.close();
		return userMissions;
	}
	
	public static JSONArray getMissionsUsers(int[] usersId) throws JSONException, SQLException {
		return getMissionsUsers(usersId, 0);
	}
	
	public static JSONArray getMissionsUsers(int[] usersId, int max_value) throws JSONException, SQLException {
		MongoWrap wrap = DataBase.getMongoCollection("mission");
		MongoCollection<Document> mission = wrap.collection;
		Document query = new Document();
		
		query.append("user id", new Document("$in", usersId));
		
		MongoCursor<Document> msg = mission.find(query).iterator();
		
		
		JSONArray userMissions = new JSONArray();
		int cpt_mess = 0;
		while(msg.hasNext() && (cpt_mess < max_value)){
			cpt_mess++;
			
			JSONObject json = new JSONObject();
			JSONObject auteur = new JSONObject();
			Document document = msg.next();
			
			
			auteur.put("user id", document.get("user id"));
			auteur.put("login", bd.Util.getUserLogin(Integer.parseInt(document.get("user id").toString())));
			
			
			json.put("author", auteur);
			json.put("mission_id", document.get("_id"));
			json.put("domaine", document.get("domaine"));
			json.put("titre", document.get("titre"));
			json.put("description", document.get("description"));
			json.put("content", document.get("content"));
			json.put("date", document.get("date"));
			
			json.put("statut", document.get("statut"));
			
			json.put("reponses", getReponse(document));
			
			
			
			
			userMissions.put(json);
		}
		
		wrap.conn.close();
		return userMissions;
	}
	
	
	public static JSONArray getAllMissions(int max_value) throws JSONException, SQLException {
		MongoWrap wrap = DataBase.getMongoCollection("mission");
		MongoCollection<Document> mission = wrap.collection;
		MongoCursor<Document> msg = mission.find().iterator();
		
		JSONArray userMissions = new JSONArray();
		int cpt_mess = 0;
		while(msg.hasNext() && (cpt_mess < max_value)){
			JSONObject json = new JSONObject();
			JSONObject auteur = new JSONObject();
			Document document = msg.next();
			
			if(document.get("parent")!=null) continue;
			
			auteur.put("user id", document.get("user id"));
			auteur.put("login", bd.Util.getUserLogin(Integer.parseInt(document.get("user id").toString())));
			json.put("author", auteur);
			json.put("mission_id", document.get("_id"));
			json.put("domaine", document.get("domaine"));
			json.put("titre", document.get("titre"));
			json.put("description", document.get("description"));
			json.put("content", document.get("content"));
			json.put("date", document.get("date"));
			
			json.put("statut", document.get("statut"));
			
			json.put("reponses", getReponse(document));
			
			
			userMissions.put(json);
		}
		
		wrap.conn.close();
		return userMissions;
	}
	
	public static JSONArray searchMissions(int id_user, String mission_query) throws JSONException, SQLException, NumberFormatException {
		String[] words = mission_query.split(" ");
		
		String regex = "(";
		for (int i = 0; i < words.length; i++) {
			if(words[i]!=""){
				regex+=words[i];
				if(i<words.length-1)
					regex+="|";
			}
		}
		regex += ")";
		MongoWrap wrap = DataBase.getMongoCollection("mission");
		MongoCollection<Document> mission = wrap.collection;
		Document query = new Document();
		
		//query.put("user id", new BasicDBObject("$ne", id_user));
		
		Document reg = new Document();
		reg.append("$regex", regex);
		reg.append("$options", "i");
		query.append("content", reg);
		
		MongoCursor<Document> msg = mission.find(query).iterator();
		
		JSONArray userMissions = new JSONArray();
		while(msg.hasNext()){
			JSONObject json = new JSONObject();
			JSONObject auteur = new JSONObject();
			Document document = msg.next();
			auteur.put("user id", document.get("user id"));
			auteur.put("login", bd.Util.getUserLogin(Integer.parseInt(document.get("user id").toString())));
			
			
			json.put("author", auteur); 
			json.put("mission_id", document.get("_id"));
			json.put("content", document.get("content"));
			json.put("date", document.get("date"));
			json.put("statut", document.get("statut"));
			
			json.put("reponses", getReponse(document));
			
			
			if(document.get("parent")!=null){
				String idparent = (String) document.get("parent");
				json.put("parent", idparent);
				JSONObject mission_parent = getMissionById(idparent);
				json.put("parent_author", mission_parent.get("author"));
			}
			userMissions.put(json);
		}
		wrap.conn.close();
		
		return userMissions;
	}

	
	
}
