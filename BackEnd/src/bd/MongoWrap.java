package bd;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoWrap {
	public MongoCollection<Document> collection;
	public MongoClient conn;
	
	public MongoWrap(MongoCollection<Document> collection, MongoClient conn) {
		this.collection = collection;
		this.conn = conn;
	}
}
