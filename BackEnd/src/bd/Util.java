package bd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import tools.RandomString;

public class Util {
	public static boolean checkUserinDB(String login) throws SQLException {
		
		Connection conn = DataBase.getMySQLConnection();
		try {
			
			String query = "SELECT * FROM user WHERE login='" + login + "';";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			boolean userExists = false;
			while(rs.next()) {
				userExists = true;
			}
			conn.close();
			return userExists;
			
		} catch (SQLException e) {
			throw e;
		} finally {
			conn.close();
		}
	}
	
	public static void addNote(int id1, int id2, int value) throws SQLException{
		Connection conn = DataBase.getMySQLConnection();
		try {
			String query = "INSERT INTO note(id1,id2,value) VALUES ('"+id1+"','"+id2+"','"+value+"');";
			
			Statement st = conn.createStatement();
			st.executeUpdate(query);
			} catch (SQLException e) {
				throw e;
			} finally {
				conn.close();
			}
	}
	
	public static double getNote(int id) throws SQLException{
		Connection conn = DataBase.getMySQLConnection();
		try {
			
			String query = "SELECT value FROM note WHERE id2='" + id + "';";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			double summ = 0;
			int cpt = 0;
			while(rs.next()) {
				summ += rs.getInt(1);
				cpt++;
			}
			
			conn.close();
			return summ/((double)cpt);
			
		} catch (SQLException e) {
			throw e;
		} finally {
			conn.close();
		}
	}
	
	public static boolean checkUserinDB(int id) throws SQLException {
		Connection conn = DataBase.getMySQLConnection();
		try {
			
			String query = "SELECT * FROM user WHERE id=" + id + ";";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			boolean userExists = false;
			while(rs.next()) {
				userExists = true;
			}
			conn.close();
			return userExists;
			
		} catch (SQLException e) {
			throw e;
		} finally {
			conn.close();
		}
	}
	
	
	public static void insertUserinDB(String login,String pw, String prenom, String nom, String domaine) throws SQLException{
		Connection conn = DataBase.getMySQLConnection();
		try {
			
			
			
			String query = "INSERT INTO user(login,password,prenom,nom,domaine) VALUES ('"+login+"','"+pw+"','"+prenom+"','"+nom+"','"+domaine+"');";
			
			Statement st = conn.createStatement();
			st.executeUpdate(query);
			} catch (SQLException e) {
				throw e;
			} finally {
				conn.close();
			}
	}
	
	
	public static int getUserID(String login) throws SQLException {
		int userID = -1;
		Connection conn = DataBase.getMySQLConnection();
		try {
			
			String query = "SELECT id FROM user WHERE login='" + login + "';";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			while(rs.next()) {
				userID = rs.getInt(1);
			}
			
		} catch (SQLException e) {
			throw e;
		} finally {
			conn.close();
		}
		
		return userID;
	}
	
	public static String getUserDomaine(int id) throws SQLException {
		String res = null;
		Connection conn = DataBase.getMySQLConnection();
		try {
			String query = "SELECT domaine FROM user WHERE id='" + id + "';";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			while(rs.next()) {
				res = rs.getString(1);
			}
			
		} catch (SQLException e) {
			throw e; 
		} finally {
			conn.close();
		}
		return res;
		
	}
	
	public static String getUserLogin(int id) throws SQLException {
		String res = null;
		Connection conn = DataBase.getMySQLConnection();
		try {
			String query = "SELECT login FROM user WHERE id='" + id + "';";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			while(rs.next()) {
				res = rs.getString(1);
			}
			
		} catch (SQLException e) {
			throw e;
		} finally {
			conn.close();
		}
		return res;
		
	}
	
	public static int getUserIDfromKey(String key) throws SQLException {
		int userID = -1;
		
		Connection conn = DataBase.getMySQLConnection();
		try {	
			String query = "SELECT id FROM session WHERE sessionkey='" + key + "';";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			while(rs.next()) {
				userID = rs.getInt(1);
			}
			
		} catch (SQLException e) {
			throw e;
		} finally {
			conn.close();
		}
		
		return userID;
	}
	
	public static boolean checkUserPw(int id, String pw) throws SQLException {
		boolean isOK = false;
		Connection conn = DataBase.getMySQLConnection();
		try {
			String query = "SELECT * FROM user WHERE id='" + id + "' and password = '" + pw + "';";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			while(rs.next()) {
				isOK = true;
			}
			
		} catch (SQLException e) {
			throw e;
		} finally {
			conn.close();
		}
		
		return isOK;
	}
	
	public static String insertConnection(int id,boolean root) throws SQLException {
		RandomString session = new RandomString();
		
		String key = session.nextString();
		Connection conn = DataBase.getMySQLConnection();
		try {
			
			
			String query = "INSERT INTO session(sessionkey,id,root) VALUES ('"+key+"',"+id+","+root+");";
			
			Statement st = conn.createStatement();
			st.executeUpdate(query);
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			conn.close();
		}
		
		return key;
		
	}
	
	public static void removeKey(String key) throws SQLException {
		Connection conn = DataBase.getMySQLConnection();
		try {		
			String query = "DELETE FROM session WHERE sessionkey = '"+key+"';";
			
			Statement st = conn.createStatement();
			st.executeUpdate(query);
			} catch (SQLException e) {
				throw e;
			} finally {
				conn.close();
			}
		
	}


	public static boolean checkKey(String key) throws SQLException {
		boolean retour = false;
		Connection conn = DataBase.getMySQLConnection();
		try {
			String query = "SELECT * FROM session WHERE sessionkey='" + key + "';";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			while(rs.next()) {
				retour = true;
			}
			
		} catch (SQLException e) {
			throw e;
		} finally {
			conn.close();
		}
		
		return retour;
	}
}