package bd;


import java.sql.*;
import javax.naming.*;
import javax.sql.DataSource;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class DataBase {
	private DataSource dataSource;
	private static DataBase database;
	private static MongoClient mongoClient;
	
	public DataBase(String jndiname) throws SQLException{
		try {
			dataSource = (DataSource) new InitialContext().lookup("java:comp/env/" + jndiname);
		} catch(NamingException e) {
			throw new SQLException(jndiname + " is missing in JNDI! : " + e.getMessage());
		}
	}
	
	public Connection getConnection() throws SQLException{
		return dataSource.getConnection();
	}
	
	
	public static Connection getMySQLConnection() throws SQLException{
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(DBStatic.pooling==false) {
			return(DriverManager.getConnection("jdbc:mysql://" + DBStatic.host + "/" + DBStatic.bd + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",DBStatic.user,DBStatic.pw));
		}
		

		else {
			if(database==null) {
				database = new DataBase("jdbc/db");
			}
			return database.getConnection();
		}
	}
	
	public static MongoWrap getMongoCollection(String nom_collection){
		
		
		MongoClientURI uri = new MongoClientURI("mongodb://"+ DBStatic.mongo_host);
	    mongoClient = new MongoClient(uri);
		MongoDatabase mdb = mongoClient.getDatabase(DBStatic.mongo_db);
			
		
		return new MongoWrap(mdb.getCollection(nom_collection),mongoClient);
	}

}
