package tools;
import org.json.JSONObject;

public class ServicesTools {

	public static JSONObject serviceRefused(String message, int codeErreur) {
		JSONObject retour = new JSONObject();
		try {
			retour.put("status", codeErreur);
			retour.put("desc", message);
		}
		catch(Exception e) {
		}
		return retour;
	}
		
	public static JSONObject serviceAccepted() {
		JSONObject retour = new JSONObject();
		try {
			retour.put("status", "OK");
		}
		catch(Exception e) {
		}
		return retour;
	}
}

