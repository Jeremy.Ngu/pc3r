package services;

import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tools.ServicesTools;

public class Missions {
	public static int MAX = 10;
	
	public static JSONObject addMission(String key, String domaine, String titre, String description, String content){
		try {	
			if (key == null || key == "")
				return ServicesTools.serviceRefused("Erreur de clé", 4);
			
			
				if(!bd.Util.checkKey(key)) {
					return ServicesTools.serviceRefused("Clé invalide",-1);
				}
			
			
			String id_mission = bd.MongoUtil.addMission(key, domaine, titre, description,content);
			
			JSONObject json = ServicesTools.serviceAccepted();
			JSONObject json_mission = bd.MongoUtil.getMissionById(id_mission);
			json.put("mission", json_mission);
			
			return json;
		} catch (SQLException e) {
			return ServicesTools.serviceRefused("Erreur SQL",1000);
		} catch (JSONException e) {
			return ServicesTools.serviceRefused("Erreur JSON",100);
		}
	}
	
	public static JSONObject removeMission(String key, String id_mission){
		if (key == null || key == "")
			return ServicesTools.serviceRefused("Erreur de clé", 4);
		
		try {
			bd.MongoUtil.removeMission(key, id_mission);
			return ServicesTools.serviceAccepted();	
		}
		catch (SQLException e) {
			return ServicesTools.serviceRefused("Erreur SQL",1000);
		}
	}
	
	/*
	 Plusieurs types de mission 
	 	"none" = tous les missions de tous les utilisateurs
	 	"user" = mission de l'utilisateur courant
	 	"friends" = mission des amis follow
	 */
	
	public static JSONObject getMissions(String key){
		return getMissions("none", key, MAX);
	}
	
	public static JSONObject getMissions(String key, int nb_mission){
		return getMissions("none", key, nb_mission);
	}
	
	public static JSONObject getMissions(String type, String key){
		return getMissions(type, key, MAX);
	}
	
	public static JSONObject getMissions(String type, String key, int nb_mission){
		return getMissions(type, key, nb_mission, -1);
	}
	
	public static JSONObject getAllMissions() {
		JSONObject json = ServicesTools.serviceAccepted();
		try {
			json.put("missions",bd.MongoUtil.getAllMissions(20));
		} catch (SQLException e) {
			return ServicesTools.serviceRefused("Erreur SQL" + e.toString(),1000);
		} catch (JSONException e) {
			return ServicesTools.serviceRefused("Erreur JSON",100);
		}
		return json;
		
	}
	
	public static JSONObject getMissions(String type, String key, int nb_mission, int id_user){
		
		try {
			
		JSONObject json = ServicesTools.serviceAccepted();
		
		switch (type) {
		
		case "none":
			json.put("missions", bd.MongoUtil.getAllMissions(nb_mission));
			break;
		
		case "user":
			if(bd.Util.checkUserinDB(id_user)){
				//Trigger mise � jour cl� session
				bd.Util.getUserIDfromKey(key);
				
				json.put("missions", bd.MongoUtil.getMissionsUser(id_user, nb_mission));
			} else
				json.put("missions", bd.MongoUtil.getMissionsUser(key, nb_mission));
			break;

		default:
			json = ServicesTools.serviceRefused("Type de mission non valide // Type demandé : None/User/Friend", -1);
			break;
		}
		
		return json;
		} catch (SQLException e) {
			return ServicesTools.serviceRefused("Erreur SQL",1000);
		} catch (JSONException e) {
			return ServicesTools.serviceRefused("Erreur JSON",100);
		}
	}
}
