package services;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import tools.ServicesTools;

public class Reponse {
	
	public static JSONObject addReponse(String key, String text, String parent) {
		if(key == null || text == null) {
			return ServicesTools.serviceRefused("Argument invalides",-1);
		}
		
		try {
			if(!bd.Util.checkKey(key)) {
				return ServicesTools.serviceRefused("Clé invalide",-1);
			}			
			
			bd.MongoUtil.addReponse(key,text,parent);
			return ServicesTools.serviceAccepted();
		
		
		
		} catch (SQLException e) {
			return ServicesTools.serviceRefused("SQL ERROR",1000);
		}
		
		

		
		
	}
	
	public static JSONObject removeReponse(String key,String mess_id) {
		if (key == null || key == "")
			return ServicesTools.serviceRefused("Erreur de clé", 4);
		
		try {
			return bd.MongoUtil.removeReponse(key, mess_id);
		}
		catch (SQLException e) {
			return ServicesTools.serviceRefused("Erreur SQL",1000);
		}
		catch (JSONException e) {
			return ServicesTools.serviceRefused("Erreur JSON",100);
		}
	}

	public static JSONObject validate(String key, String bool, String mess_id) {
		if(key == null || bool == null) {
			return ServicesTools.serviceRefused("Argument invalides",-1);
		}
		
		try {
			if(!bd.Util.checkKey(key)) {
				return ServicesTools.serviceRefused("Clé invalide",-1);
			}
			
			JSONObject retour;
			
			retour = bd.MongoUtil.validate(key,bool,mess_id);
			return retour;
			}catch (SQLException e) {
			return ServicesTools.serviceRefused("SQL ERROR",1000);}
			catch(JSONException e) {
				return ServicesTools.serviceRefused("JSON ERROR", 100);
			}
	}
}
