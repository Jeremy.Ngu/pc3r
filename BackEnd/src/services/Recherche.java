package services;


import org.json.JSONObject;
import tools.ServicesTools;


public class Recherche {

	public static JSONObject search(String login, String query, String friends) {
		if(login == null || query == null) {
			return ServicesTools.serviceRefused("Argument invalides",-1);
		}
		if(friends == null) {
			return ServicesTools.serviceRefused("Argument invalides",-1);
		}
		
		
		return ServicesTools.serviceAccepted();
	
	}
		
}
