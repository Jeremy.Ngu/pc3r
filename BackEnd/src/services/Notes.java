package services;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import tools.ServicesTools;

public class Notes {
	
	public static JSONObject getNote(String id) {
		
		try {
			JSONObject retour = ServicesTools.serviceAccepted();
			int id_int = Integer.parseInt(id);
			
			if(!bd.Util.checkUserinDB(id_int)) {
				return ServicesTools.serviceRefused("Cannot find user : " , 6);
			}
			else {
				retour.put("value",bd.Util.getNote(id_int));
				return retour;
			}
		}catch (SQLException e) {
			return ServicesTools.serviceRefused("Erreur SQL",1000);
		} catch (JSONException e) {
			return ServicesTools.serviceRefused("Erreur JSON",100);
		}

	}
	
	public static JSONObject addNote(String key, String id, String parent, String value){
		try {	
			int id1 = bd.Util.getUserIDfromKey(key);
			int id2 = Integer.parseInt(id);
			int note = Integer.parseInt(value);
			
			if (key == null || key == "")
				return ServicesTools.serviceRefused("Erreur de clé", 4);
			
			
				if(!bd.Util.checkKey(key)) {
					return ServicesTools.serviceRefused("Clé invalide",-1);
				}
			if(note < 0 || note > 5) {
				return ServicesTools.serviceRefused("Value invalid", 5);
			}
			
			if(!bd.Util.checkUserinDB(id2)) {
				return ServicesTools.serviceRefused("Cannot find user : " , 6);
			}
			
			
			if(!bd.MongoUtil.userAlreadyNoted(key, parent)) {
				bd.Util.addNote(id1,id2,note);
				bd.MongoUtil.addUserNoted(key, parent, note);
				JSONObject json = ServicesTools.serviceAccepted();
				return json;
			}
			
			
			return ServicesTools.serviceRefused("Erreur JAVA",10);
			
		} catch (SQLException e) {
			return ServicesTools.serviceRefused("Erreur SQL",1000);
		}
	}
}
