package services;

import java.security.Provider.Service;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.util.JSON;

import bd.DataBase;

public class TEST2 {
	
	public static void main(String args[]) {
		

		try {
			
			String id_mess = bd.MongoUtil.addMission("62gEnex4sbazWh8O3cqz7", "Maths", "Equation", "Resoudre une équation", "2x^2 = 8");
			String id_resp1 = bd.MongoUtil.addReponse("ee6236cca3184d579aa489a64902d508","2",id_mess);
			String id_resp2 = bd.MongoUtil.addReponse("EyxAwDdevmjfd7eSZE6JW","-2",id_mess);
			String id_resp3 = bd.MongoUtil.addReponse("62gEnex4sbazWh8O3cqz7", "3424124", id_mess);
			

			String id_mess2 = bd.MongoUtil.addMission("EyxAwDdevmjfd7eSZE6JW", "Tarkov", "Leveling", "1-40", "Why does Jaeger quests are so ridiculously difficult");
			String id_resp4 = bd.MongoUtil.addReponse("ee6236cca3184d579aa489a64902d508","git gud",id_mess2);
			String id_resp5 = bd.MongoUtil.addReponse("62gEnex4sbazWh8O3cqz7", "mosin boy", id_mess2);
			
			bd.MongoUtil.validate("EyxAwDdevmjfd7eSZE6JW", "false", id_resp4);
			bd.MongoUtil.validate("EyxAwDdevmjfd7eSZE6JW", "true", id_resp5);
			
			
			String id_mess3 = bd.MongoUtil.addMission("EyxAwDdevmjfd7eSZE6JW", "Info", "Web Application", "Deployment", "How can I use effectively JavaScript");
			String id_resp7 = bd.MongoUtil.addReponse("62gEnex4sbazWh8O3cqz7", "All you have to do is go to 3I017", id_mess3);
			String id_resp8 = bd.MongoUtil.addReponse("EyxAwDdevmjfd7eSZE6JW","Nevermind, just found out on my own",id_mess3);
			bd.MongoUtil.validate("EyxAwDdevmjfd7eSZE6JW", "true", id_resp7);
			bd.MongoUtil.validate("EyxAwDdevmjfd7eSZE6JW", "true", id_resp8);
			
			String id_mess4 = bd.MongoUtil.addMission("ee6236cca3184d579aa489a64902d508", "Maths", "Conjecture", "Solve this", "Anyone could give me a hand on the twin prime conjecture ?");

			JSONArray json = bd.MongoUtil.getAllMissions(50);
			System.out.println(json.toString());
			
			


			
			
			
		} catch (SQLException | JSONException e1) {
			e1.printStackTrace();
		}
		finally {
			
		}
		
		
		
		
			
		System.out.println("Done");
		
	}
}		
	
	
		
		

