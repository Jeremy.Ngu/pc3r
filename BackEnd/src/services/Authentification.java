package services;


import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;
import tools.ServicesTools;


public class Authentification {

	public static JSONObject createUser(String login, String pw, String prenom, String nom, String domaine) {
		if(login == null) {
			return ServicesTools.serviceRefused("Argument invalides (login)",-1);
			
		}
		if(pw == null) {
			return ServicesTools.serviceRefused("Argument invalides (pw)",-1);
		}
		
		boolean verification;
		
		try {
			verification = bd.Util.checkUserinDB(login); 
			if(verification) {
				return ServicesTools.serviceRefused("Utilisateur déjà enregistré", 1000);
			}
			
			bd.Util.insertUserinDB(login,pw,prenom,nom,domaine);
			return ServicesTools.serviceAccepted();
			
		}catch(SQLException e) {
			return ServicesTools.serviceRefused(e.toString(), 1000);
		}
		
		
		
	}
	
	
	public static JSONObject login(String login, String pw) {
		if(login == null || pw == null) {
			return ServicesTools.serviceRefused("Argument invalides",-1);
		}
		
		boolean verification;
		
		try {
			verification = bd.Util.checkUserinDB(login);
			if(!verification) {
				return ServicesTools.serviceRefused("Utilisateur non enregistré", 1000);
			}
			
			// R�cup�rer l'ID de l'user
			int id = bd.Util.getUserID(login);
			
			verification = bd.Util.checkUserPw(id, pw);
			if(!verification) {
				return ServicesTools.serviceRefused("Bad Password", -1);
			}
			
			String domaine = bd.Util.getUserDomaine(id);
			
			boolean root = true;
			String key = bd.Util.insertConnection(id,root);
			
			JSONObject retour = ServicesTools.serviceAccepted();
			
			
			retour.put("id", id);
			retour.put("login", login);
			retour.put("key", key);
			retour.put("domaine", domaine);
			
			return retour;
			
		} catch(SQLException e) {
			return ServicesTools.serviceRefused(e.toString(), 1000);
		} catch (JSONException e) {
			return ServicesTools.serviceRefused("Erreur JSON", 100);
		}
		// return ServicesTools.serviceRefused("Erreur de Java", 10000);	
	}
	
	
	public static JSONObject logout(String key) {
		if(key == null) {
			return ServicesTools.serviceRefused("Argument invalides",-1);
		}
		
		try {
			bd.Util.removeKey(key);
		} catch (SQLException e) {
			return ServicesTools.serviceRefused(e.toString(), 1000);
		}
		return ServicesTools.serviceAccepted();
		
		
	}
	
	
}
