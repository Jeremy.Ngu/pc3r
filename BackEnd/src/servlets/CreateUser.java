package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

public class CreateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CreateUser() {
        super();
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String login = request.getParameter("login");
		String pw = request.getParameter("pw");
		String prenom = request.getParameter("prenom");
		String nom = request.getParameter("nom");
		String domaine = request.getParameter("domaine");
		
		
		
		JSONObject resp = services.Authentification.createUser(login, pw, prenom, nom, domaine);
		
		
		
		PrintWriter out = response.getWriter();
		out.print(resp.toString());
		
		
	}

}
