package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class AddMessage
 */
@WebServlet("/addMessage")
public class AddMission extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddMission() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String key = request.getParameter("key");
		String domaine = request.getParameter("domaine");
		String titre = request.getParameter("titre");
		String description = request.getParameter("description");
		String probleme = request.getParameter("probleme");
		
		
		//Parent = message parent si on veut mettre des commentaires retweet etc...
		//String parent = (request.getParameter("parent")!=null && request.getParameter("parent")!="") ? request.getParameter("parent") : "" ;
		
		PrintWriter out = response.getWriter();
		
		JSONObject json;
		json = services.Missions.addMission(key, domaine,titre,description,probleme);
			
		response.setContentType("text/plain");
		out.print(json.toString());
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
