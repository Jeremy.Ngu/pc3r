package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

public class AddReponse extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public AddReponse() {
        super();
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String key = request.getParameter("key");
		String content = request.getParameter("content");
		String parent = request.getParameter("parent");
		
		
		JSONObject resp = services.Reponse.addReponse(key,content,parent);
		
		
		
		PrintWriter out = response.getWriter();
		out.print(resp.toString());
		
		
	}

}
